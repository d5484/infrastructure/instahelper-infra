#!/bin/bash

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/33246898/terraform/state/instahelper-infra_<env>" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/33246898/terraform/state/instahelper-infra_<env>/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/33246898/terraform/state/instahelper-infra_<env>/lock" \
    -backend-config="username=NickGlebanov" \
    -backend-config="password=<token>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
