
resource "cloudflare_record" "record" {
  for_each = var.domains

  name    = each.value
  value   = aws_lb.main.dns_name
  type    = "CNAME"
  proxied = true
  zone_id = var.cloudflare_zone_id
}