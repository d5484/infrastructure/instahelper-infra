terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.7.0"
    }
  }
  backend "http" {}

}


# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = var.aws_zone
}

provider "cloudflare" {
  email     = var.email
  api_token = var.cloudflare_api_token
}


output "alb_dns_name" {
  value       = aws_lb.main.dns_name
  description = "The domain name of the load balancer"
}
