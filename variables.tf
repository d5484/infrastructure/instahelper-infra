

variable "email" {
  type = string
}

variable "aws_zone" {
  default = "eu-west-1"
  type    = string
}

variable "cloudflare_api_token" {
  type = string
}

variable "cloudflare_zone_id" {
  type = string
}


variable "domains" {
  type = map(string)
}

variable "env" {
  type = string
}
